package com.github.guod.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/18 时间:15:32
 * @JDK 1.8
 * @Description 功能模块：
 */
@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class OpenServiceConfigApplication {
    public static void main(String[] args) {
        SpringApplication.run(OpenServiceConfigApplication.class,args);
    }
}
