package com.github.guod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/18 时间:11:51
 * @JDK 1.8
 * @Description 功能模块：
 */
@SpringBootApplication
@EnableEurekaServer
public class OpenServiceRegistryApplication {
    public static void main(String[] args) {
        SpringApplication.run(OpenServiceRegistryApplication.class, args);
    }
}
