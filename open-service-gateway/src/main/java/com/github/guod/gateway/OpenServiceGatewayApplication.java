package com.github.guod.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/18 时间:15:57
 * @JDK 1.8
 * @Description 功能模块：
 */
@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication
public class OpenServiceGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(OpenServiceGatewayApplication.class, args);
    }
}
